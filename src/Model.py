from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Index, Text
import uuid
import datetime

BaseModel = declarative_base()


def _generate_uuid() -> str:
    return uuid.uuid1().hex


# 定义User对象:
class User(BaseModel):
    # 表的名字:
    __tablename__ = 'users'
    
    # 表的结构:
    account = Column(String(64), primary_key=True)
    auth = Column(Integer, default=0)
    type = Column(Integer, default=0)
    nickname = Column(String(64))
    password = Column(String(64))
    contact = Column(String(32))
    regTime = Column(DateTime, default=datetime.datetime.now)
    lastTime = Column(DateTime, default=datetime.datetime.now)
    salt = Column(String(8))
    other = Column(Text)

    def __repr__(self) -> dict:
        dic = {
            'account': self.account, 'nickname': self.nickname, 'auth': self.auth,
            'regTime': str(self.regTime), 'lastTime': str(self.lastTime), 'other': self.other
        }
        return str(dic)


# 定义登录对像
class Token(BaseModel):
    # 表的名字:
    __tablename__ = 'tokens'

    # 表的结构:
    token = Column(String(32), default=_generate_uuid, primary_key=True)
    account = Column(String(64), index=True)
    auth = Column(Integer)
    type = Column(Integer)
    nickname = Column(String(64))
    contact = Column(String(32))
    regTime = Column(DateTime)
    lastTime = Column(DateTime, default=datetime.datetime.now)
    other = Column(Text)

    def __repr__(self):
        dic = {
            'account': self.account, 'token': self.token, 'nickname': self.nickname, 'auth': self.auth,
            'regTime': str(self.regTime), 'lastTime': str(self.lastTime), 'other': self.other
        }
        return str(dic)
