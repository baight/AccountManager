import datetime
import random
import hashlib
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from Model import BaseModel, User, Token


def generate_a_salt():
    """
    随机返回一个盐字符串
    :return:
    """
    all_visible_char = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghijklmnopqrstuvwxyz{|}~"
    return "".join(random.sample(all_visible_char, 8))


def generate_final_password(password, salt) -> str:
    """
    返回 密码 和 盐 生成的 md5值
    :param password: 密码
    :param salt: 盐
    :return: md5值
    """
    return hashlib.sha1((password+salt).encode()).hexdigest()


class AccountManager:
    """
    AccountManager 不做权限检查
    """
    def __init__(self, url: str, echo=False):
        """
        创建管理对象
        :param url:
            "sqlite://"                             # 内存
            "sqlite:///:memory:"                    # 内存
            "sqlite:///C:/path/to/account.db"       # 绝对路径
            "sqlite:///account.db"                  # 相对路径
            "mysql+pymysql://account:password@localhost/database?charset=utf8"
        """
        self.engine = sqlalchemy.create_engine(url, echo=echo)
        # 创建所有表
        BaseModel.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def register(self, account: str, password: str) -> Token:
        old_user = self.session.query(User).filter(User.account == account).first()
        if old_user:
            raise Exception(f'{account} 已经被注册过了')

        user = User()
        user.account = account
        user.nickname = account
        user.salt = generate_a_salt()
        user.password = generate_final_password(password, user.salt)
        try:
            self.session.add(user)
            self.session.commit()

            login = Token()
            login.account = user.account
            login.nickname = user.nickname
            login.auth = user.auth
            login.type = user.type
            login.lastTime = user.lastTime
            login.regTime = user.regTime
            login.other = user.other
            self.session.add(login)

            self.session.commit()
        except Exception as e:
            self.session.rollback()
            print(user)
            raise e
        return login

    def login(self, account: str, password: str) -> Token:
        user = self.session.query(User).filter(User.account == account).first()
        if not user:
            raise Exception(f'{account} 账号不存在')

        final_password = generate_final_password(password, user.salt)
        if final_password != user.password:
            raise Exception(f'密码不正确')

        user.lastTime = datetime.datetime.now()
        login = Token()
        login.account = user.account
        login.nickname = user.nickname
        login.auth = user.auth
        login.type = user.type
        login.lastTime = user.lastTime
        login.regTime = user.regTime
        login.other = user.other
        try:
            self.session.add(login)
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            print(user)
            raise e
        return login

    def user_info(self, token: str) -> Token:
        login = self.session.query(Token).filter(Token.token == token).first()
        return login

    def logout(self, account: str) -> None:
        try:
            self.session.query(Token).filter(Token.account == account).delete()
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            print(account)
            raise e

    def update(self, account: str, **kwargs) -> User:
        user = self.session.query(User).filter(User.account == account).first()
        login_list = self.session.query(Token).filter(Token.account == account)
        if not user:
            raise Exception(f'{account} 账号不存在')
        for key in kwargs:
            value = kwargs[key]
            setattr(user, key, value)
            for login in login_list:
                setattr(login, key, value)
        try:
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            print(login)
            raise e
        return user

    def delete(self, account: str) -> None:
        try:
            self.session.query(User).filter(User.account == account).delete()
            self.session.query(Token).filter(Token.account == account).delete()
            self.session.commit()
        except Exception as e:
            self.session.rollback()
            print(account)
            raise e


if __name__ == "__main__":
    manager = AccountManager("sqlite:///account.db")
    # r = manager.register("baight", "111111")
    # print(r)
    r = manager.update("baight", auth=80)
    print(r)
